Docker Image Publish
====================

A script to build, tag and publish docker images with version labels and dev suffix, base on the source git branch.
It can be configured by env vars or `pakege.json` entries.

The env vars
------------

* `APP_NAME`
* `VERSION`
* `PRODUCTION_BRANCH`
* `DOCKER_REPO`: can be a comma separated list of repositories.
* `DOCKER_GROUP`
* `IS_LATEST`: accepts `true`, `false` or empty.

The equivalent configuration in a `pakege.json`
-----------------------------------------------

```js
{
  "name": "app-name", // Will be used if it's not a git repo with a remote named "origin".
  "version": "0.1.2",
  ...
  "docker-repo": "registry.example.com,gcr.io/my-id",
  "docker-group": "group-name",
  "production-branch": "master",
  "scripts": {
    ...
    "docker:build:info": "docker-build-info",
    "docker:build": "npm run -s build && docker-build",
    "docker:push": "docker-push",
    "docker:release": "npm run -s docker:build && docker-push",
    "docker:release:latest": "npm run -s docker:build --latest && docker-push --latest",
    "docker:run": ". docker-commom; docker run -p ${PORT:-8080}:80 $DOCKER_IMG",
    "docker:run:sh": ". docker-commom; docker run -p ${PORT:-8080}:80 -it $DOCKER_IMG sh",
  }
  ...
}
```

Optional features enabled by cli arguments
------------------------------------------

* `--latest`: Adds a "latest" tag to the publishing list.
* `--append-commit-hash`: Adds the current git's commit hash to the full version tag.
* `--append-timestamp`: Adds the current UTC time to the full version tag.

Using without installing
------------------------

Useful for tasks inside GitLab-CI and other ephemeral environments.

```sh
curl https://ntopus.gitlab.io/docker-image-publish/publish.sh |
APP_NAME=my-app VERSION=0.0.0 PRODUCTION_BRANCH=main \
DOCKER_REPO=docker.io DOCKER_GROUP=my-name | sh -e
```

This will run `docker-build-info && docker-build && docker-push`

Debugging
---------

If you want to know what you will get published, you can run `docker-build-info` with any optional argument. The generate tags will be used by `docker-build` and `docker-push`.

Running without arguments will result in something alike:
```
• From version 0.1.2 was parsed: MAJOR:0 MINOR:1 PATCH:2
• The production branch is master, so it has no sufix.
• It will building docker image: app-name:0.1.2
• The repos to publish are:
  • registry.example.com
  • gcr.io/my-id
• The tags are:
  • group-name/app-name:0
  • group-name/app-name:0.1
  • group-name/app-name:0.1.2
```
